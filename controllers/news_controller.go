package controllers

import (
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/b0ralgin/news_aggreagator/controllers/utils"
	"bitbucket.org/b0ralgin/news_aggreagator/services"
	echo "github.com/labstack/echo"
)

type NewsController struct {
	storage *services.Storage
}

func NewController(storage *services.Storage) *NewsController {
	return &NewsController{
		storage: storage,
	}
}

func (n *NewsController) AddPortalWithRSS(c echo.Context) error {
	params := utils.JsonRSS{}
	if err := c.Bind(&params); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	rssService := services.NewRssAggreagator(params.Url, n.storage.GetStorageChan())
	go rssService.RunAggregator(time.Duration(int64(time.Second) * params.Update))
	return c.JSON(http.StatusCreated, nil)
}

func (n *NewsController) AddPortalWithHTML(c echo.Context) error {
	params := utils.JsonHTML{}
	if err := c.Bind(&params); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	rssService := services.NewHtmlAggregator(
		params.Url,
		n.storage.GetStorageChan(),
		params.Selectors,
	)
	go rssService.RunAggregator(time.Duration(int64(time.Second) * params.Update))
	return c.JSON(http.StatusCreated, nil)
}

func (n *NewsController) GetArticles(perPage int) func(c echo.Context) error {
	if perPage <= 0 {
		perPage = 10
	}
	return func(c echo.Context) error {
		offset, err := strconv.ParseInt(c.QueryParam("page"), 10, 64)
		if err != nil {
			offset = 0
		}

		query := c.QueryParam("search")
		articles, err := n.storage.GetArticles(perPage, int(offset), query)
		if err != nil {
			return c.JSON(http.StatusInternalServerError, err)
		}
		return c.JSON(http.StatusOK, articles)
	}
}
