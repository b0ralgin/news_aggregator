package utils

type RequestBody struct {
	Url    string `json:"url"`
	Update int64  `json:"refresh"`
}

type JsonRSS RequestBody
type Selectors struct {
	LinkSelector        string `json:"link_selector"`
	TitleSelector       string `json:"title_selector"`
	ContentSelector     string `json:"content_selector"`
	PublishedAtSelector string `json:"publish_selector"`
}

type JsonHTML struct {
	RequestBody
	Selectors
}
