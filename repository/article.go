package repository

import (
	"bitbucket.org/b0ralgin/news_aggreagator/models"
	pg "gopkg.in/pg.v5"
)

func AddArticle(article models.Article, db *pg.DB) error {
	_, err := db.Model(&article).
		Where("source = ? AND published_at = ?", article.Source, article.PublishedAt).
		OnConflict("DO NOTHING").
		SelectOrInsert()
	return err
}

func GetLastArticle(source string, db *pg.DB) (models.Article, error) {
	var lastArticle models.Article
	err := db.Model(&lastArticle).Where("source = ?", source).Order("published_at DESC").First()
	return lastArticle, err
}

func GetArticles(limit int, offset int, db *pg.DB) ([]models.Article, error) {
	articles := []models.Article{}
	err := db.Model(&articles).
		Order("published_at").
		Offset(offset * limit).
		Limit(limit).
		Select()
	return articles, err
}

func GetArticlesBy(limit int, offset int, query string, db *pg.DB) ([]models.Article, error) {
	articles := []models.Article{}
	err := db.Model(&articles).
		Where("make_tsvector(title) @@ to_tsquery(?)", query).
		Order("published_at").
		Offset(offset * limit).
		Limit(limit).
		Select()
	return articles, err
}
