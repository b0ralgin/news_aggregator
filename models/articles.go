package models

import "time"

type Article struct {
	ID          int64 `json:"-"`
	Source      string
	Title       string
	Content     string
	PublishedAt time.Time
}
