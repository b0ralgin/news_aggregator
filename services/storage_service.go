package services

import (
	"log"
	"strings"

	"bitbucket.org/b0ralgin/news_aggreagator/models"
	"bitbucket.org/b0ralgin/news_aggreagator/repository"
	pg "gopkg.in/pg.v5"
)

type Storage struct {
	db      *pg.DB
	storage chan models.Article
	logger  log.Logger
}

func NewStorage(db *pg.DB) *Storage {
	storage := &Storage{
		db:      db,
		storage: make(chan models.Article),
	}
	go storage.runListener()
	return storage
}

func (s *Storage) GetStorageChan() chan models.Article {
	return s.storage
}

func (s *Storage) runListener() {
	for article := range s.storage {
		if err := repository.AddArticle(article, s.db); err != nil {
			s.logger.Println("Error in saving article:", err)
		}
	}
}

func (s *Storage) GetArticles(limit int, offset int, query string) ([]models.Article, error) {
	if query != "" {
		query = strings.Replace(query, " ", "<->", -1)
		return repository.GetArticlesBy(limit, offset, query, s.db)
	}
	return repository.GetArticles(limit, offset, s.db)
}

func (s *Storage) Close() {
	s.db.Close()
	close(s.storage)
}
