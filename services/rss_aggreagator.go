package services

import (
	"log"
	"time"

	"bitbucket.org/b0ralgin/news_aggreagator/models"
	"github.com/mmcdole/gofeed"
)

type RssAggreagator struct {
	url     string
	storage chan models.Article
	logger  log.Logger
}

func NewRssAggreagator(url string, storage chan models.Article) *RssAggreagator {
	return &RssAggreagator{
		url:     url,
		storage: storage,
	}
}

func (r *RssAggreagator) RunAggregator(period time.Duration) {
	c := time.NewTicker(period).C
	for _ = range c {
		if articles, err := r.FetchData(); err != nil {
			r.logger.Println("cannot get data from RSS ", r.url, err)
		} else {
			for _, feed := range articles {
				article := models.Article{
					Title:   feed.Title,
					Source:  r.url,
					Content: feed.Description,
				}
				if feed.PublishedParsed != nil {
					article.PublishedAt = *feed.PublishedParsed
				} else {
					article.PublishedAt = time.Now()
				}
				r.storage <- article
			}
		}
	}
}

func (r *RssAggreagator) FetchData() ([]*gofeed.Item, error) {
	fp := gofeed.NewParser()
	feed, err := fp.ParseURL(r.url)
	if err != nil {
		return nil, err
	}
	return feed.Items, nil
}
