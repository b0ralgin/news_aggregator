package services

import (
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"bitbucket.org/b0ralgin/news_aggreagator/controllers/utils"
	"bitbucket.org/b0ralgin/news_aggreagator/models"
	"github.com/PuerkitoBio/goquery"
)

type HtmlAggregator struct {
	url       string
	storage   chan models.Article
	client    *http.Client
	selectors utils.Selectors
}

func NewHtmlAggregator(url string, storage chan models.Article, selectors utils.Selectors) *HtmlAggregator {
	return &HtmlAggregator{
		url:       url,
		storage:   storage,
		selectors: selectors,
		client: &http.Client{
			Timeout:   time.Second * 10,
			Transport: &http.Transport{},
		},
	}
}

func (h *HtmlAggregator) RunAggregator(period time.Duration) {
	c := time.NewTicker(period).C
	var wg sync.WaitGroup
	for _ = range c {
		urlChan := make(chan string, 10)
		go h.addArticle(urlChan, h.storage, &wg)
		if err := h.FetchData(urlChan); err != nil {
			log.Println("cannot get data from RSS ", h.url, err)
		}
		close(urlChan)
		wg.Wait()
	}
}

func (h *HtmlAggregator) FetchData(urlChan chan<- string) error {
	resp, err := h.client.Get(h.url)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		return err
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return err
	}

	doc.Find(h.selectors.LinkSelector).Each(func(i int, s *goquery.Selection) {
		if url, exists := s.Attr("href"); exists {
			urlChan <- url
		}
	})
	return nil
}

func (h *HtmlAggregator) addArticle(urlChan <-chan string, storage chan<- models.Article, wg *sync.WaitGroup) {
	for u := range urlChan {
		wg.Add(1)
		go func() {
			base, _ := url.Parse(h.url)
			linkUrl, _ := url.Parse(u)
			article, err := h.parseArticle(h.client, base.ResolveReference(linkUrl).String())
			if err != nil {
				log.Println("Cannot parse article", err)
			} else {
				article.Source = h.url
				storage <- article
			}
			wg.Done()
		}()
	}
}

func (h *HtmlAggregator) parseArticle(client *http.Client, url string) (models.Article, error) {
	log.Println(url)
	article := models.Article{}
	resp, err := h.client.Get(url)
	if resp != nil {
		defer resp.Body.Close()
	}
	if err != nil {
		log.Println(err)
		return article, err
	}
	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		return article, err
	}
	title := doc.Find(h.selectors.TitleSelector).Text()
	content := doc.Find(h.selectors.ContentSelector).Text()
	publishAt, err := time.Parse("hh.mm", doc.Find(h.selectors.PublishedAtSelector).Text())
	if err == nil {
		now := time.Now()
		publishAt.AddDate(now.Year(), int(now.Month()), now.Day())
	} else {
		publishAt = time.Now()
	}
	article = models.Article{
		Title:       title,
		Content:     content,
		PublishedAt: publishAt,
	}
	return article, nil
}
