package main

import (
	"flag"
	"log"
	"os"
	"time"

	"bitbucket.org/b0ralgin/news_aggreagator/config"
	"bitbucket.org/b0ralgin/news_aggreagator/controllers"
	"bitbucket.org/b0ralgin/news_aggreagator/services"
	echo "github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	pg "gopkg.in/pg.v5"
)

var (
	cnfFilename = flag.String("config", "config/config.yaml", "Filename of config file (YML format)")
)

func main() {

	flag.Parse()

	logger := log.New(os.Stdout, "news-aggreagtor", log.LstdFlags)

	config, err := config.LoadCfg(*cnfFilename)
	if err != nil {
		logger.Fatal("cannot parse config file:", err)
	}

	db, err := openDB(config.DB, config.LogMode, logger)

	e := echo.New()
	e.Use(middleware.Logger())
	storage := services.NewStorage(db)
	defer storage.Close()
	newsController := controllers.NewController(storage)
	e.POST("/rss", newsController.AddPortalWithRSS)
	e.POST("/html", newsController.AddPortalWithHTML)
	e.GET("/", newsController.GetArticles(config.PerPage))

	e.Logger.Fatal(e.Start(config.Port))
}

func openDB(conn string, debugMode bool, logger *log.Logger) (*pg.DB, error) {
	opts, err := pg.ParseURL(conn)
	if err != nil {
		return nil, err
	}
	db := pg.Connect(opts)

	if debugMode {
		pg.SetQueryLogger(logger)
	}
	var st time.Time
	_, err = db.QueryOne(pg.Scan(&st), "set timezone TO 'GMT'; SELECT now()")
	return db, err
}
